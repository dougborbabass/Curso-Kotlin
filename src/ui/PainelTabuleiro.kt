package ui

import modelo.Tabuleiro
import BotaoCampo
import java.awt.GridLayout
import javax.swing.JPanel


class PainelTabuleiro(tabuleiro: Tabuleiro) : JPanel() {

    init {
        layout = GridLayout(tabuleiro.qtdeLinhas, tabuleiro.qtdeColunas)
        tabuleiro.forEachCampo { Campo ->
            val botao = BotaoCampo(Campo)
            add(botao)
        }
    }
}